FROM python:3.6

RUN pip install better_exceptions
ENV BETTER_EXCEPTIONS=1

RUN mkdir -p /src /data /output/reports /output/charts /output/www /input

COPY heater_logs /src/heater_logs
COPY LICENSE README.md setup.py /src/

WORKDIR /src
RUN python setup.py build

RUN pip install --no-cache-dir -e /src/
#RUN pip install --no-cache-dir /src/

WORKDIR /output

ENTRYPOINT ["python", "-m", "heater_logs"]
