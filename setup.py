import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="heater_logs",
    version="2.3.0",
    author="Tom Paton",
    author_email="tom.paton@gmail.com",
    description="heater_logs module for cyclesbot",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.com/tompaton/heater_logs",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
    install_requires=[
        'paho-mqtt==1.4.0',
        'matplotlib==3.0.0',
        'astral==1.7.1',
        'scipy==1.2.0',
    ],
)
