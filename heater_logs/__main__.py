import argparse
import os
import shlex
from datetime import datetime
from pathlib import Path

from . import database, history, charts, capture


def import_messages_from_args(args):
    with database.init_db() as conn:
        if args.filename:
            for ts, topic, message in history.import_messages(args.filename,
                                                              args.legacy):
                conn.insert_message(ts, topic, message)
        else:
            for log in Path('/input').glob('*.log*'):
                for ts, topic, message in history.import_messages(log.open()):
                    conn.insert_message(ts, topic, message)


def summary_from_args(args):
    show = args.show or ['dates', 'topics']

    with database.init_db() as conn:
        if 'topics' in show:
            print('\n'.join(f'{count:6d} {size:10d} {topic}'
                            for topic, count, size in conn.count_topics()))

        if 'dates' in show:
            print('\n'.join(f'{count:6d} {date}'
                            for date, count in conn.count_dates()))


def extract_messages_from_args(args):
    with database.init_db() as conn:
        place_id = conn.get_place_id(args.place)

        if args.clear:
            conn.clear_data(place_id)

        conn.extract_data(place_id, args.place)


def capture_messages_from_args(args):
    with database.init_db() as conn:
        def on_message(ts, topic, message):
            print(f'[{ts:%Y-%m-%dT%H:%M:%S}] {topic} {message}')

            conn.insert_new_message(ts, topic, message)

        capture.capture(os.getenv('MQTT_SERVER'), int(os.getenv('MQTT_PORT')),
                        os.getenv('MQTT_USER'), os.getenv('MQTT_PASSWORD'),
                        on_message)


def chart_from_args(args):
    if args.date is None:
        date = datetime.now().strftime('%Y-%m-%d')
    else:
        date = args.date

    with database.init_db() as conn:
        place = conn.get_place(args.place)

        if args.chart == 'radar_day':
            day_data = conn.get_day_data(date, place['place_id'])

            print(charts.radar_day(day_data,
                                   datetime.strptime(date, '%Y-%m-%d'),
                                   f'{place["key"]}-day-{date}', place['name'],
                                   place['latitude'], place['longitude']))

        elif args.chart == 'garden_week':
            week_data = conn.get_week_data(date, place['place_id'])

            print(charts.garden_week(week_data,
                                     datetime.strptime(date, '%Y-%m-%d'),
                                     f'{place["key"]}-garden-{date}',
                                     place['name'],
                                     place['latitude'], place['longitude']))

        elif args.chart == 'house_history':
            house_data = conn.get_house_data(date, place['place_id'])

            print(charts.house_history(house_data,
                                       datetime.strptime(date, '%Y-%m-%d'),
                                       f'{place["key"]}-house-{date}',
                                       place['name']))


class HeaterLogArgumentParser(argparse.ArgumentParser):

    def convert_arg_line_to_args(self, arg_line):
        if arg_line.strip().startswith('#'):
            return []

        return shlex.split(arg_line)


parser = HeaterLogArgumentParser(fromfile_prefix_chars='@')
subparsers = parser.add_subparsers(dest='func_name')

parser_import = subparsers.add_parser('import')
parser_import.set_defaults(func=import_messages_from_args)
parser_import.add_argument('--filename', type=argparse.FileType('r'))
parser_import.add_argument('--legacy', action='store_true')

parser_extract = subparsers.add_parser('extract')
parser_extract.set_defaults(func=extract_messages_from_args)
parser_extract.add_argument('--clear', action='store_true')
parser_extract.add_argument('--place', required=True,
                            help='extract for place name')

parser_capture = subparsers.add_parser('capture')
parser_capture.set_defaults(func=capture_messages_from_args)

parser_summary = subparsers.add_parser('summary')
parser_summary.set_defaults(func=summary_from_args)
parser_summary.add_argument('--show', action='append',
                            choices=['dates', 'topics'])

parser_chart = subparsers.add_parser('chart', aliases=['graph'])
parser_chart.set_defaults(func=chart_from_args)
parser_chart.add_argument('--place', required=True)
parser_chart.add_argument('--date')
parser_chart.add_argument('--chart', action='store', default='radar_day',
                          choices=['radar_day', 'garden_week', 'house_history'])


if __name__ == '__main__':
    args = parser.parse_args()
    if args.func_name:
        args.func(args)
    else:
        parser.print_help()
