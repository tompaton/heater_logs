import contextlib
import sqlite3
from collections import namedtuple
from datetime import datetime, timedelta
from . import analysis


def insert_new_message(ts, topic, message):
    with init_db(timeout=30, create_tables=False, attach_permanent=False) as db:
        db.insert_new_message(ts, topic, message)


@contextlib.contextmanager
def init_db(timeout=5, create_tables=True,
            attach_permanent=True, attach_ephemeral=True):

    with sqlite3.connect('/data/heater_logs_config.db',
                         timeout=timeout) as conn:
        conn.row_factory = sqlite3.Row

        if create_tables or attach_permanent:
            conn.execute("ATTACH DATABASE '/data/heater_logs_permanent.db' "
                         "AS permanent")

        if create_tables or attach_ephemeral:
            conn.execute("ATTACH DATABASE '/data/heater_logs_ephemeral.db' "
                         "AS ephemeral")

        if create_tables:
            make_schema(conn)

        yield HeaterDB(conn)


def make_schema(conn):

    # CONFIG (main)

    conn.execute("""CREATE TABLE IF NOT EXISTS
                    main.places (
                      place_id INTEGER PRIMARY KEY,
                      key TEXT,
                      name TEXT,
                      latitude NUMERIC,
                      longitude NUMERIC,
                      UNIQUE (key)
                    )""")

    conn.execute("""CREATE TABLE IF NOT EXISTS
                    main.rooms (
                      room_id INTEGER PRIMARY KEY,
                      place_id INTEGER,
                      key TEXT,
                      sequence INTEGER,
                      name TEXT,
                      colour TEXT,
                      charts TEXT,
                      UNIQUE (place_id, key, sequence)
                    )""")

    conn.execute("""CREATE TABLE IF NOT EXISTS
                    main.devices (
                      device_id INTEGER PRIMARY KEY,
                      place_id INTEGER,
                      key TEXT,
                      sequence INTEGER,
                      name TEXT,
                      colour TEXT,
                      charts TEXT,
                      UNIQUE (place_id, key, sequence)
                    )""")

    # topics in config so inserting a new message doesn't require
    # attaching the permanent (extracted) data file
    conn.execute("""CREATE TABLE IF NOT EXISTS
                    main.topics (
                      topic_id INTEGER PRIMARY KEY,
                      topic TEXT,
                      UNIQUE (topic)
                    )""")

    # PERMANENT DATA

    conn.execute("""CREATE TABLE IF NOT EXISTS
                    permanent.temperatures (
                      raw_id INTEGER,
                      room_id INTEGER,
                      ts INTEGER,
                      value NUMERIC,
                      UNIQUE (room_id, ts)
                    )""")

    conn.execute("""CREATE TABLE IF NOT EXISTS
                    permanent.humidity (
                      raw_id INTEGER,
                      room_id INTEGER,
                      ts INTEGER,
                      value NUMERIC,
                      UNIQUE (room_id, ts)
                    )""")

    conn.execute("""CREATE TABLE IF NOT EXISTS
                    permanent.moisture (
                      raw_id INTEGER,
                      room_id INTEGER,
                      ts INTEGER,
                      value NUMERIC,
                      UNIQUE (room_id, ts)
                    )""")

    conn.execute("""CREATE TABLE IF NOT EXISTS
                    permanent.pressure (
                      raw_id INTEGER,
                      room_id INTEGER,
                      ts INTEGER,
                      value NUMERIC,
                      UNIQUE (room_id, ts)
                    )""")

    conn.execute("""CREATE TABLE IF NOT EXISTS
                    permanent.duty_cycles (
                      raw_id INTEGER,
                      device_id INTEGER,
                      ts INTEGER,
                      relay TEXT,
                      duration INTEGER,
                      UNIQUE (device_id, ts)
                    )""")

    conn.execute("""CREATE TABLE IF NOT EXISTS
                    permanent.targets (
                      raw_id INTEGER,
                      device_id INTEGER,
                      ts INTEGER,
                      value INTEGER,
                      UNIQUE (device_id, ts)
                    )""")

    conn.execute("""CREATE TABLE IF NOT EXISTS
                    permanent.rainfall (
                      raw_id INTEGER,
                      place_id INTEGER,
                      ts INTEGER,
                      value INTEGER,
                      UNIQUE (place_id, ts)
                    )""")

    conn.execute("""CREATE TABLE IF NOT EXISTS
                    permanent.forecasts (
                      fetched INTEGER,
                      place_id INTEGER,
                      ts INTEGER,
                      temperature NUMERIC,
                      UNIQUE (fetched, place_id, ts)
                    )""")

    # EPHEMERAL DATA (rollover per month?)

    conn.execute("""CREATE TABLE IF NOT EXISTS
                    ephemeral.raw_logs (
                      raw_id INTEGER PRIMARY KEY,
                      ts INTEGER,
                      topic_id INTEGER,
                      message TEXT,
                      UNIQUE (ts, topic_id, message)
                    )""")


class HeaterDB:
    def __init__(self, conn):
        self.conn = conn

    def insert_new_message(self, ts, topic, message):
        self.insert_message(ts, topic, message)
        self.conn.commit()

    def insert_message(self, ts, topic, message):
        topic_id = self.get_topic_id(topic)
        try:
            return _insert_row(self.conn.cursor(), 'raw_logs',
                               ts=ts, topic_id=topic_id, message=message)
        except sqlite3.IntegrityError:
            # TODO: return rowid?
            return None

    def get_topic_id(self, topic):
        try:
            cur = self.conn.cursor()
            cur.execute("SELECT topic_id FROM topics WHERE topic = ?",
                        (topic,))
            return cur.fetchone()['topic_id']
        except:
            return _insert_row(self.conn.cursor(), 'topics', topic=topic)

    def get_place_id(self, place):
        cur = self.conn.cursor()
        cur.execute("SELECT place_id FROM places WHERE key = ?",
                    (place,))
        return cur.fetchone()['place_id']

    def get_place(self, place):
        cur = self.conn.cursor()
        cur.execute("SELECT place_id, key, name, latitude, longitude "
                    "FROM places WHERE key = ?",
                    (place,))
        return cur.fetchone()

    def get_room_id(self, place_id, room):
        cur = self.conn.cursor()
        cur.execute("SELECT room_id FROM rooms WHERE place_id = ? AND key = ?",
                    (place_id, room))
        return cur.fetchone()['room_id']

    def get_rooms(self, place_id, chart=None):
        cur = self.conn.cursor()
        if chart is None:
            sql = ("SELECT room_id, key FROM rooms "
                   "WHERE place_id = ? "
                   "ORDER BY sequence")
        else:
            sql = (f"SELECT room_id, key, name, colour FROM rooms "
                   f"WHERE place_id = ? "
                   f"AND charts LIKE '%{chart}%' "
                   f"ORDER BY sequence")

        cur.execute(sql, (place_id,))
        return cur.fetchall()

    def get_device_id(self, place_id, device):
        cur = self.conn.cursor()
        cur.execute("SELECT device_id FROM devices WHERE place_id = ? AND key = ?",
                    (place_id, device))
        return cur.fetchone()['device_id']

    def get_devices(self, place_id, chart=None):
        cur = self.conn.cursor()
        if chart is None:
            sql = ("SELECT device_id, key FROM devices "
                   "WHERE place_id = ? "
                   "ORDER BY sequence")
        else:
            sql = (f"SELECT device_id, key, name, colour FROM devices "
                   f"WHERE place_id = ? "
                   f"AND charts LIKE '%{chart}%'"
                   f"ORDER BY sequence")

        cur.execute(sql, (place_id,))
        return cur.fetchall()

    def insert_forecast(self, fetched, place_id, ts, temperature):
        try:
            return _insert_row(self.conn.cursor(), 'forecasts',
                               fetched=fetched, place_id=place_id, ts=ts,
                               temperature=temperature)
        except sqlite3.IntegrityError:
            # TODO: return rowid?
            return None

    def count_topics(self):
        cur = self.conn.cursor()
        cur.execute("SELECT topics.topic, COUNT(raw_logs.raw_id), "
                    "SUM(LENGTH(raw_logs.message)) "
                    "FROM raw_logs LEFT OUTER JOIN topics "
                    "ON raw_logs.topic_id = topics.topic_id "
                    "GROUP BY topics.topic ORDER BY 2 DESC")
        return cur.fetchall()

    def count_dates(self):
        cur = self.conn.cursor()
        cur.execute("SELECT SUBSTR(ts, 0, 11), COUNT(*) FROM raw_logs "
                    "GROUP BY 1 ORDER BY 1")
        return cur.fetchall()

    def clear_data(self, place_id):
        self.clear_temperatures(place_id)
        self.clear_humidity(place_id)
        self.clear_moisture(place_id)
        self.clear_pressure(place_id)
        self.clear_duty_cycles(place_id)
        self.clear_targets(place_id)

    def extract_data(self, place_id, place_key):
        for room_id, room_key in self.get_rooms(place_id):
            self.extract_temperatures(
                self.get_topic_id(f'places/{place_key}/{room_key}/temperature'),
                room_id)
            self.extract_humidity(
                self.get_topic_id(f'places/{place_key}/{room_key}/humidity'),
                room_id)
            self.extract_moisture(
                self.get_topic_id(f'places/{place_key}/{room_key}/soil-moisture'),
                room_id)
            self.extract_pressure(
                self.get_topic_id(f'places/{place_key}/{room_key}/pressure'),
                room_id)

        for device_id, device_key in self.get_devices(place_id):
            self.extract_duty_cycles(
                self.get_topic_id(f'places/{place_key}/{device_key}/relay/duty'),
                device_id)

            self.extract_targets(
                self.get_topic_id(f'places/{place_key}/{device_key}/target'),
                device_id)

        self.extract_rainfall(
            self.get_topic_id(f'places/{place_key}/rain/raw'),
            place_id)

    def clear_temperatures(self, place_id):
        self.conn.execute('DELETE FROM temperatures WHERE room_id IN ('
                          'SELECT room_id FROM rooms WHERE place_id=?'
                          ')',
                          (place_id,))

    def extract_temperatures(self, topic_id, room_id, raw_id=None):
        sql = ('INSERT INTO temperatures (raw_id, ts, room_id, value) '
               'SELECT raw_id, ts, ?, message FROM raw_logs '
               'WHERE topic_id = ? ')

        if raw_id is None:
            sql += 'AND raw_id NOT IN (SELECT raw_id FROM temperatures)'
            params = (room_id, topic_id)
        else:
            sql += 'AND raw_id = ?'
            params = (room_id, topic_id, raw_id)

        self.conn.execute(sql, params)

    def clear_humidity(self, place_id):
        self.conn.execute('DELETE FROM humidity WHERE room_id IN ('
                          'SELECT room_id FROM rooms WHERE place_id=?'
                          ')',
                          (place_id,))

    def extract_humidity(self, topic_id, room_id):
        sql = ("INSERT INTO humidity (raw_id, ts, room_id, value) "
               "SELECT raw_id, ts, ?, message FROM raw_logs "
               "WHERE topic_id = ? "
               "AND raw_id NOT IN (SELECT raw_id FROM humidity)")
        self.conn.execute(sql, (room_id, topic_id))

    def clear_moisture(self, place_id):
        self.conn.execute('DELETE FROM moisture WHERE room_id IN ('
                          'SELECT room_id FROM rooms WHERE place_id=?'
                          ')',
                          (place_id,))

    def extract_moisture(self, topic_id, room_id, raw_id=None):
        sql = ("INSERT INTO moisture (raw_id, ts, room_id, value) "
               "SELECT raw_id, ts, ?, message FROM raw_logs "
               "WHERE topic_id = ? ")

        if raw_id is None:
            sql += 'AND raw_id NOT IN (SELECT raw_id FROM moisture)'
            params = (room_id, topic_id)
        else:
            sql += 'AND raw_id = ?'
            params = (room_id, topic_id, raw_id)

        self.conn.execute(sql, params)

    def clear_pressure(self, place_id):
        self.conn.execute('DELETE FROM pressure WHERE room_id IN ('
                          'SELECT room_id FROM rooms WHERE place_id=?'
                          ')',
                          (place_id,))

    def extract_pressure(self, topic_id, room_id):
        sql = ("INSERT INTO pressure (raw_id, ts, room_id, value) "
               "SELECT raw_id, ts, ?, message FROM raw_logs "
               "WHERE topic_id = ? "
               "AND raw_id NOT IN (SELECT raw_id FROM pressure)")
        self.conn.execute(sql, (room_id, topic_id))

    def clear_duty_cycles(self, place_id):
        self.conn.execute('DELETE FROM duty_cycles WHERE device_id IN ('
                          'SELECT device_id FROM devices WHERE place_id=?'
                          ')',
                          (place_id,))

    def extract_duty_cycles(self, topic_id, device_id, raw_id=None):
        sql = ("INSERT INTO duty_cycles "
               "(raw_id, device_id, ts, relay, duration) "
               "SELECT raw_id, ?, ts, json_extract(message, '$.value'), "
               " json_extract(message, '$.duration') "
               "FROM raw_logs WHERE topic_id = ? ")

        if raw_id is None:
            sql += "AND raw_id NOT IN (SELECT raw_id FROM duty_cycles)"
            params = (device_id, topic_id,)
        else:
            sql += 'AND raw_id = ?'
            params = (device_id, topic_id, raw_id)

        self.conn.execute(sql, params)

    def clear_targets(self, place_id):
        self.conn.execute('DELETE FROM targets WHERE device_id IN ('
                          'SELECT device_id FROM devices WHERE place_id=?'
                          ')',
                          (place_id,))

    def extract_targets(self, topic_id, device_id, raw_id=None):
        sql = ("INSERT INTO targets (raw_id, device_id, ts, value) "
               "SELECT raw_id, ?, ts, message "
               "FROM raw_logs WHERE topic_id = ? ")

        if raw_id is None:
            sql += "AND raw_id NOT IN (SELECT raw_id FROM targets)"
            params = (device_id, topic_id,)
        else:
            sql += 'AND raw_id = ?'
            params = (device_id, topic_id, raw_id)

        self.conn.execute(sql, params)

    def clear_rainfall(self, place_id):
        self.conn.execute('DELETE FROM rainfall WHERE place_id =?',
                          (place_id,))

    def extract_rainfall(self, topic_id, place_id, raw_id=None):
        sql = ("INSERT INTO rainfall (raw_id, place_id, ts, value) "
               "SELECT raw_id, ?, ts, message "
               "FROM raw_logs WHERE topic_id = ? ")

        if raw_id is None:
            sql += "AND raw_id NOT IN (SELECT raw_id FROM rainfall)"
            params = (place_id, topic_id,)
        else:
            sql += 'AND raw_id = ?'
            params = (place_id, topic_id, raw_id)

        self.conn.execute(sql, params)

    def get_day_data(self, date, place_id):
        day_data = self.get_one_day_data(date, place_id)

        yesterday = datetime.strptime(date, '%Y-%m-%d') - timedelta(hours=24)
        yesterday_date = yesterday.strftime('%Y-%m-%d')

        yesterday_data = self.get_one_day_data(yesterday_date, place_id,
                                               yesterday=True)

        return list(yesterday_data) + list(day_data)

    def get_one_day_data(self, date, place_id, yesterday=False):
        if not yesterday:
            yield ('forecasts', '#ffffff', False,
                   self.get_day_forecasts(date, place_id))

        for room in self.get_rooms(place_id, chart='radar_day'):
            yield ('temperatures', room['colour'], yesterday,
                   self.get_day_temperatures(date, room['room_id']))

        for device in self.get_devices(place_id, chart='radar_day'):
            yield ('duty_cycles', device['colour'], yesterday,
                   self.get_day_duty_cycles(date, device['device_id'], 'ON'))

            if not yesterday:
                yield ('targets', '#859900', yesterday,
                       self.get_day_targets(date, device['device_id']))

    def get_day_temperatures(self, date, room_id):
        cur = self.conn.cursor()
        cur.execute('SELECT ts, value FROM temperatures '
                    'WHERE ts LIKE ? AND room_id = ? ORDER BY ts',
                    (date + '%', room_id))
        return [(to_seconds(ts[11:19]), value)
                for ts, value in cur.fetchall()]

    def get_day_duty_cycles(self, date, device_id, relay):
        cur = self.conn.cursor()
        cur.execute('SELECT ts, duration FROM duty_cycles '
                    'WHERE device_id = ? AND ts LIKE ? AND relay = ? '
                    'ORDER BY ts',
                    (device_id, date + '%', relay))
        return [(to_seconds(ts[11:19]), duration)
                for ts, duration in cur.fetchall()]

    def get_day_targets(self, date, device_id):
        cur = self.conn.cursor()

        # carry over target from previous day
        cur.execute("SELECT :date, value FROM targets "
                    "WHERE device_id = :device_id AND ts IN "
                    "(SELECT MAX(ts) FROM targets "
                    "WHERE device_id = :device_id AND ts < :date) "
                    "UNION "
                    "SELECT ts, value FROM targets "
                    "WHERE device_id = :device_id "
                    "AND ts LIKE :date2 ORDER BY 1",
                    {'date': date,
                     'device_id': device_id,
                     'date2': date + '%'})

        return [(to_seconds(ts[11:19]), value)
                for ts, value in cur.fetchall()]

    def get_day_forecasts(self, date, place_id):
        cur = self.conn.cursor()
        cur.execute('SELECT ts, temperature FROM forecasts '
                    'WHERE place_id = ? AND fetched IN ('
                    'SELECT MAX(fetched) FROM forecasts WHERE ts LIKE ?'
                    ')',
                    (place_id, date + '%',))
        return [(to_seconds(ts[11:19]), temperature)
                for ts, temperature in cur.fetchall()]

    def get_week_data(self, date, place_id):
        start = datetime.strptime(date, '%Y-%m-%d') - timedelta(days=6)
        start_date = start.strftime('%Y-%m-%d')

        end = datetime.strptime(date, '%Y-%m-%d') + timedelta(days=1)
        end_date = end.strftime('%Y-%m-%d')

        week_data = []

        week_data.append(self._forecasts_series(place_id, start_date, end_date))

        week_data.append(self._rainfall_series(place_id, start_date, end_date))

        for room in self.get_rooms(place_id, chart='garden_week'):
            week_data.append(self._temperatures_series(room['room_id'],
                                                       room['colour'],
                                                       start_date, end_date))

            week_data.append(self._humidity_series(room['room_id'],
                                                   room['colour'],
                                                   start_date, end_date))

            week_data.append(self._moisture_series(room['room_id'],
                                                   room['colour'],
                                                   start_date, end_date))

            week_data.append(self._pressure_series(room['room_id'],
                                                   room['colour'],
                                                   start_date, end_date))

        return week_data

    def _forecasts_series(self, place_id, start_date, end_date,
                          label=None):
        return ChartSeries('forecasts', '#ffffff', False,
                           self.get_week_forecasts(start_date, end_date,
                                                   place_id),
                           {},
                           label)

    def _rainfall_series(self, place_id, start_date, end_date,
                         label=None):
        return ChartSeries('rainfall', '#2aa198', False,
                           self.get_week_rainfall(start_date, end_date,
                                                  place_id),
                           {},
                           label)

    def _temperatures_series(self, room_id, color, start_date, end_date,
                             label=None):
        return ChartSeries('temperatures', color, False,
                           self._get_week_data(start_date, end_date,
                                               room_id, 'temperatures'),
                           {},
                           label)

    def _humidity_series(self, room_id, color, start_date, end_date,
                         label=None):
        return ChartSeries('humidity', color, False,
                           self._get_week_data(start_date, end_date,
                                               room_id, 'humidity'),
                           {},
                           label)

    def _moisture_series(self, room_id, color, start_date, end_date,
                         label=None):
        cycles = self.get_moisture_cycles(self._get_all_data(room_id,
                                                             'moisture'))
        return ChartSeries('moisture', color, False,
                           self._get_week_data(start_date, end_date,
                                               room_id, 'moisture'),
                           {'cycles': list(cycles)},
                           label)

    def _pressure_series(self, room_id, color, start_date, end_date,
                         label=None):
        return ChartSeries('pressure', color, False,
                           self._get_week_data(start_date, end_date,
                                               room_id, 'pressure'),
                           {},
                           label)

    def _get_all_data(self, room_id, table):
        cur = self.conn.cursor()
        cur.execute(f'SELECT ts, value FROM {table} '
                    'WHERE room_id = ? ORDER BY ts',
                    (room_id,))
        return [(datetime.strptime(ts, "%Y-%m-%d %H:%M:%S.%f"), value)
                for ts, value in cur.fetchall()]

    def _get_week_data(self, start_date, end_date, room_id, table):
        cur = self.conn.cursor()
        cur.execute(f'SELECT ts, value FROM {table} '
                    'WHERE ts >= ? AND ts < ? AND room_id = ? ORDER BY ts',
                    (start_date, end_date, room_id))
        return [(datetime.strptime(ts, "%Y-%m-%d %H:%M:%S.%f"), value)
                if '.' in ts
                else (datetime.strptime(ts, "%Y-%m-%d %H:%M:%S"), value)
                for ts, value in cur.fetchall()]

    def get_week_forecasts(self, start_date, end_date, place_id):
        cur = self.conn.cursor()
        # TODO: only latest fetched forecast per ts
        cur.execute('SELECT ts, temperature FROM forecasts '
                    'WHERE place_id = ? AND ts >= ? AND ts < ?',
                    (place_id, start_date, end_date))
        return [(datetime.strptime(ts, "%Y-%m-%d %H:%M:%S"), temperature)
                for ts, temperature in cur.fetchall()]

    def get_week_rainfall(self, start_date, end_date, place_id):
        cur = self.conn.cursor()
        cur.execute('SELECT ts, value FROM rainfall '
                    'WHERE place_id = ? AND ts >= ? AND ts < ?',
                    (place_id, start_date, end_date))
        return [(datetime.strptime(ts, "%Y-%m-%d %H:%M:%S.%f"), value)
                for ts, value in cur.fetchall()]

    def get_moisture_cycles(self, data):
        if not data:
            return

        cuts = analysis.find_cuts(data)

        for t1, t2 in zip(cuts, cuts[1:]):
            v1, v2 = analysis.best_fit([t for t, v in data if t1 <= t <= t2],
                                       [v for t, v in data if t1 <= t <= t2])
            if v1 > v2:
                yield ((t1, v1), (t2, v2))

    def get_house_data(self, date, place_id):
        start_date = '2000-01-01'

        end = datetime.strptime(date, '%Y-%m-%d') + timedelta(days=1)
        end_date = end.strftime('%Y-%m-%d')

        house_data = []

        for room in self.get_rooms(place_id, chart='house_history'):
            house_data.append(self._temperatures_series(room['room_id'],
                                                        room['colour'],
                                                        start_date, end_date,
                                                        room['name']))

            house_data.append(self._humidity_series(room['room_id'],
                                                    room['colour'],
                                                    start_date, end_date,
                                                    room['name']))

        return house_data


def _insert_row(cur, table, **fields):
    cur.execute("INSERT INTO {} ({}) VALUES ({})"
                .format(table,
                        ', '.join(f'"{field}"' for field in fields),
                        ', '.join('?' for field in fields)),
                tuple(fields.values()))

    return cur.lastrowid


def to_seconds(hhmmss):
    if ':' in hhmmss:
        hh, mm, ss = hhmmss.split(':')
        return int(hh) * 3600 + int(mm) * 60 + int(ss)
    else:
        return 0


ChartSeries = namedtuple('ChartSeries',
                         'series_type color secondary data extra label')
