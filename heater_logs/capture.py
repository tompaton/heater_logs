from datetime import datetime
import paho.mqtt.client as mqtt


def capture(mqtt_server, mqtt_port, mqtt_user, mqtt_password, callback):
    client = mqtt.Client()

    def on_connect(client_, userdata, flags, rc):
        # print('on_connect')
        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        client_.subscribe("#")


    def on_message(client_, userdata, msg):
        # print(f'on_message {msg.topic}')
        callback(datetime.now(), msg.topic, msg.payload.decode('utf-8'))

    client.on_connect = on_connect
    client.on_message = on_message

    client.username_pw_set(mqtt_user, password=mqtt_password)

    client.connect(mqtt_server, mqtt_port, 60)

    client.loop_forever()
