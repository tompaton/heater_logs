# coding: utf-8

import math
import os

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib import gridspec

from astral import Location
from datetime import datetime, timedelta, time

from . import analysis


BG_COLOR = '#fdf6e3'


def radar_day(day_data, date, filename, title, latitude, longitude):
    plt.style.use('Solarize_Light2')
    fig = radar_day_fig(day_data, date, latitude, longitude)
    if title:
        fig.suptitle(title, fontsize=16, x=0.02, ha='left')
    fname = f'/output/charts/{filename}.png'
    fig.savefig(fname, facecolor=BG_COLOR, edgecolor=BG_COLOR)
    return fname


def radar_day_fig(day_data, date, latitude, longitude):
    s = 24 * 60 * 60 / math.tau

    all_v = [v for series_type, color, secondary, data in day_data
             if series_type in ('temperatures', 'forecasts', 'targets')
             for t, v in data
             if v]

    min_v = int(min(all_v or [15]))
    max_v = int(max(all_v or [25])) + 1

    fig, ax = polar_24hr_temperature(min_v, max_v,
                                     rorigin=min_v - 0.5 * (max_v - min_v))

    # background shading

    for t1, t2 in sunset_sunrise_sectors(date, latitude, longitude):
        ax.fill([t1 / s, t2 / s, t2 / s, t1 / s],
                [min_v-2, min_v-2, max_v+2, max_v+2],
                facecolor='#888888',
                edgecolor=BG_COLOR,
                alpha=0.2)

    # background shading - degrees

    z = list(range(min_v, max_v))
    normal = plt.Normalize(10, 40)  # mid point at 25°C
    cmap = plt.cm.coolwarm(normal(z))

    for t in z:
        X = [i*15 / s for i in range(0, 24 * 60 * 60 // 15)]
        ax.fill(X + list(reversed(X)),
                [t] * len(X) + [t+1] * len(X),
                facecolor=cmap[t - min_v], edgecolor=BG_COLOR,
                alpha=0.25)

    for series_type, color, secondary, data in day_data:
        if series_type == 'temperatures':
            if secondary:
                plot_temperature_yesterday(ax, data, color, s)
            else:
                plot_temperature(ax, data, color, s)

        elif series_type == 'forecasts':
            plot_forecast(ax, data, color, s)

        elif series_type == 'duty_cycles':
            if secondary:
                plot_duty_cycles_yesterday(ax, data, max_v - 0.5, color, s)
            else:
                plot_duty_cycles(ax, data, max_v - 0.5, color, s)

        elif series_type == 'targets':
            for ts0, target0, ts1, target1 in target_series(data):
                plot_line(ax, ts0, target0, ts1, target1, 15 * 60, color, s)

    return fig


def polar_24hr_temperature(min_v, max_v, rorigin=0.0,
                           width=1000, height=1000, dpi=100,
                           margin=0.05):
    fig = plt.figure(figsize=(width/dpi, height/dpi), dpi=dpi)
    ax = fig.add_subplot(111, projection='polar')

    ax.set_position([margin, margin, 1.0 - 2 * margin, 1.0 - 2 * margin])
    ax.set_ylim(bottom=min_v, top=max_v)
    ax.set_rorigin(rorigin)
    ax.set_theta_zero_location('N')
    ax.set_theta_direction('clockwise')
    ax.set_xticks([math.tau * i / 24 for i in range(24)])
    ax.set_xticklabels([f'{h:02d}:00' for h in range(24)])
    ax.yaxis.set_major_formatter(ticker.FormatStrFormatter("%d°C"))
    ax.set_rlabel_position(3)

    # additional r axis labels
    polar_twin(ax).set_rlabel_position(120 + 2)
    polar_twin(ax).set_rlabel_position(240)

    return fig, ax


def polar_twin(ax):
    ax2 = ax.figure.add_axes(ax.get_position(),
                             projection='polar',
                             label='twin',
                             frameon=False,
                             theta_direction=ax.get_theta_direction(),
                             theta_offset=ax.get_theta_offset())
    ax2.xaxis.set_visible(False)
    ax2.grid(False)
    ax2.set_rorigin(ax.get_rorigin())
    ax2.set_ylim(ax.get_ylim())
    ax2.yaxis.set_major_formatter(ax.yaxis.get_major_formatter())

    return ax2


def sunset_sunrise_sectors(date, latitude, longitude, altitude=0):
    sun = Location(('location', 'region', latitude, longitude,
                    os.environ['TZ'], altitude)).sun(date=date)
    sunrise = seconds_since_midnight(sun['sunrise'])
    sunset = seconds_since_midnight(sun['sunset'])

    X = []
    for h in range(12 * 60 * 60, (24 + 12) * 60 * 60, 60 * 60):
        if h > sunset and h < (24 - 1) * 60 * 60 + sunrise:
            X.append((h, h + 60 * 60))

    X.insert(0, (sunset, X[0][0]))
    X.insert(-1, (X[-1][1], sunrise))

    return X


def seconds_since_midnight(dt):
    delta = dt - dt.replace(hour=0, minute=0, second=0, microsecond=0)
    return delta.total_seconds()


def plot_temperature(ax, data, color, s):
    ax.plot([t / s for t, v in data],
            [v for t, v in data],
            marker='o', markersize=2,
            linewidth=0.5, color=color)


def plot_temperature_yesterday(ax, data, color, s):
    ax.plot([t / s for t, v in data],
            [v for t, v in data],
            linewidth=0.5,
            color=color,
            alpha=0.75)


def plot_forecast(ax, data, color, s):
    extrapolated = list(analysis.extrapolate([(t + 30 * 60, v)
                                              for t, v in data]))

    ax.plot([t / s for t, v in extrapolated],
            [v for t, v in extrapolated],
            linewidth=2, color=color, alpha=0.75)


def plot_duty_cycles(ax, data, r, color, s, d=1/3):
    ax.scatter([t / s for t, v in data],
               [r for t, v in data],
               s=[d * v for t, v in data],
               c=color,
               alpha=0.7)


def plot_duty_cycles_yesterday(ax, data, r, color, s, d=1/3):
    ax.scatter([t / s for t, v in data],
               [r for t, v in data],
               s=[d * v for t, v in data],
               c=color,
               alpha=0.25)


def target_series(data):
    if data:
        targets = zip(data, data[1:] + [(24 * 60 * 60 - 1, data[-1][1])])

        for ((ts0, target0), (ts1, target1)) in targets:
            yield ts0, target0, ts1, target1


def plot_line(ax, x0, y0, x1, y1, step, color, s):
    X = list(range(x0, x1, step)) + [x1]

    ax.plot([t / s for t in X],
            [y0] * len(X),
            color=color,
            linewidth=1)


def garden_week(week_data, date, filename, title, latitude, longitude):
    plt.style.use('Solarize_Light2')
    fig = garden_week_fig(week_data, date, latitude, longitude)
    if title:
        fig.suptitle(title, fontsize=16, x=0.02, ha='left')
    fname = f'/output/charts/{filename}.png'
    fig.savefig(fname, facecolor=BG_COLOR, edgecolor=BG_COLOR)
    return fname


def garden_week_fig(week_data, date, latitude, longitude):
    width = 2000
    height = 1000
    dpi = 100

    all_t = [v for series in week_data
             for t, v in series[3]
             if series[0] in ('temperatures', 'forecasts')]
    min_t, max_t = int(min(all_t or [15])), int(max(all_t or [25]) + 1)

    min_p, max_p = 0, 100
    min_pressure, max_pressure = 990, 1040

    dates = {ts.date() for series in week_data
             for ts, v in series[3]
             if series[0] == 'forecasts'} or {date.date()}
    min_d = datetime.combine(min(dates), time(0, 0, 0))
    max_d = datetime.combine(max(dates), time(23, 59, 59))
    dates.add(min_d.date() - timedelta(days=1))
    dates.add(max_d.date() + timedelta(days=1))

    fig = plt.figure(figsize=(width/dpi, height/dpi), dpi=dpi)

    gs = gridspec.GridSpec(2, 1, height_ratios=[2, 1])

    ax_temp = plt.subplot(gs[0])
    ax_temp.tick_params(left=True, labelleft=True, right=True, labelright=True)
    ax_temp.set_ylabel('Temperature °C')

    ax_temp.set_ylim(bottom=min_t, top=max_t)
    ax_temp.set_xlim(left=min_d, right=max_d)
    ax_temp.xaxis.set_visible(False)

    ax_perc = plt.subplot(gs[1], label='perc')
    ax_perc.tick_params(left=True, labelleft=True,
                        right=False, labelright=False)
    ax_perc.set_ylabel('Humidity/Moisture %/Pressure hPa')
    ax_perc.set_ylim(bottom=min_p, top=max_p)
    ax_perc.set_xlim(left=min_d, right=max_d)

    ax_pressure = fig.add_subplot(gs[1], label='pressure', facecolor='none')
    ax_pressure.tick_params(left=False, labelleft=False,
                            right=True, labelright=True,
                            bottom=False, labelbottom=False)
    ax_pressure.set_ylim(bottom=min_pressure, top=max_pressure)
    ax_pressure.set_xlim(left=min_d, right=max_d)

    fig.tight_layout()
    fig.subplots_adjust(wspace=0, hspace=0.01, top=0.95)

    # TODO: remove ylabels < 0, > 100, position title
    # TODO: share gridlines betwen axes

    # TODO: date label format - major Month/Year, minor DoW/Day
    # TODO: date is labelling end-of-day (wrong tz?)

    # shade night

    for t1, t2 in sunset_sunrise_week(dates, latitude, longitude):
        ax_temp.fill([t1, t2, t2, t1],
                     [min_t, min_t, max_t, max_t],
                     facecolor='#888888',
                     edgecolor=BG_COLOR,
                     alpha=0.2)
        ax_perc.fill([t1, t2, t2, t1],
                     [min_p-2, min_p-2, max_p+2, max_p+2],
                     facecolor='#888888',
                     edgecolor=BG_COLOR,
                     alpha=0.2)

    # temperature gradients

    z = list(range(min_t, max_t))
    normal = plt.Normalize(10, 40)  # mid point at 25°C
    cmap = plt.cm.coolwarm(normal(z))

    for t in z:
        ax_temp.fill([min_d, min_d, max_d, max_d],
                     [t, t+1, t+1, t],
                     facecolor=cmap[t - min_t],
                     #edgecolor=BG_COLOR,
                     alpha=0.25)

    # humidity gradients

    step = 10
    z = list(reversed(range(min_p, max_p+1, step)))
    normal = plt.Normalize(0, 100)  # reversed, mid point at 50%
    cmap = plt.cm.gist_earth(normal(z))

    for p in z:
        ax_perc.fill([min_d, min_d, max_d, max_d],
                     [p, p+step, p+step, p],
                     facecolor=cmap[(p - min_p) // step],
                     #edgecolor=BG_COLOR,
                     alpha=0.25)

    # TODO: forecast next 3-5 days (compressed to just show min/max)

    for series in week_data:
        if not series.data:
            continue

        if series.series_type == 'temperatures':
            ax_temp.plot([t for t, v in series.data],
                         [v for t, v in series.data],
                         linewidth=0.5, color=series.color)

        elif series.series_type == 'forecasts':
            ax_temp.plot([t for t, v in series.data],
                         [v for t, v in series.data],
                         linewidth=2, color=series.color)

        elif series.series_type == 'humidity':
            ax_perc.plot([t for t, v in series.data],
                         [v for t, v in series.data],
                         linewidth=0.5, color=series.color)

        elif series.series_type == 'pressure':
            ax_pressure.plot([t for t, v in series.data],
                             [v for t, v in series.data],
                             linewidth=8, alpha=0.3,
                             color=series.color)

        elif series.series_type == 'moisture':
            ax_perc.plot([t for t, v in series.data],
                         [v for t, v in series.data],
                         linewidth=0.5, color=series.color)

            top_v = analysis.median([v1 for (t1, v1), (t2, v2)
                                     # ignore first cycle
                                     in series.extra['cycles'][1:]
                                     # filter out junk readings
                                     if v2 > 25])
            base_v = analysis.median([v2 for (t1, v1), (t2, v2)
                                      # ignore unfinished last cycle
                                      in series.extra['cycles'][:-1]
                                      # filter out junk readings
                                      if v2 > 25])

            for (t1, v1), (t2, v2) in series.extra['cycles']:
                if t2 > series.data[0][0] and t1 < series.data[-1][0]:
                    top_t = analysis.interpolate_x(t1, v1, t2, v2, top_v)
                    base_t = analysis.interpolate_x(t1, v1, t2, v2, base_v)

                    if t2 < top_t:
                        plot_line2_left(ax_perc, t1, v1, t2, v2, '#2aa198')

                    elif base_t < t1:
                        plot_line2_left(ax_perc, t1, v1, t2, v2, '#dc322f')

                    else:
                        t1, v1 = plot_line2_left(ax_perc, t1, v1, top_t, top_v,
                                                 '#2aa198')
                        t2, v2 = plot_line2_right(ax_perc, base_t, base_v, t2, v2,
                                                  '#dc322f')
                        plot_line2_left(ax_perc, t1, v1, t2, v2, series.color)

        elif series.series_type == 'rainfall':
            ax_perc.plot([t for t, v in series.data],
                         [95.0 for t, v in series.data],
                         linewidth=0,
                         marker='o', markersize=4, alpha=0.5,
                         color=series.color)

    return fig


def plot_line2_left(ax, x0, y0, x1, y1, color, alpha=0.33, linewidth=10):
    if x1 > x0:
        ax.plot([x0, x1], [y0, y1],
                color=color, alpha=alpha, linewidth=linewidth)
        return x1, y1
    else:
        return x0, y0


def plot_line2_right(ax, x0, y0, x1, y1, color, alpha=0.33, linewidth=10):
    if x1 > x0:
        ax.plot([x0, x1], [y0, y1],
                color=color, alpha=alpha, linewidth=linewidth)
        return x0, y0
    else:
        return x1, y1


def sunset_sunrise_week(dates, latitude, longitude, altitude=0):
    sun = Location(('location', 'region', latitude, longitude,
                    os.environ['TZ'], altitude))

    x = []

    for date in sorted(list(dates)):
        times = sun.sun(date=date)
        x.append((times['sunrise'], times['sunset']))

    X = []

    for d1, d2 in zip(x, x[1:]):
        X.append((d1[1].replace(tzinfo=None), d2[0].replace(tzinfo=None)))

    return X


def house_history(house_data, date, filename, title):
    plt.style.use('Solarize_Light2')
    fig = house_history_fig(house_data, date, title)
    fname = f'/output/charts/{filename}.png'
    fig.savefig(fname, facecolor=BG_COLOR, edgecolor=BG_COLOR)
    return fname


def house_history_fig(house_data, date, title):
    width = 2000
    height = 1000
    dpi = 100

    latest = max(t for series in house_data
                 for t, v in series[3]
                 if series[0] in ('temperatures',))

    midnight = datetime.combine(date.date(), time(23, 59, 59))

    ages = [
        (latest - timedelta(hours=1), '1h'),
        (latest - timedelta(hours=3), '3h'),
        (latest - timedelta(hours=6), '6h'),
        (latest - timedelta(hours=12), '12h'),
        (latest - timedelta(days=1), '1d'),
        (midnight - timedelta(days=3), '3d'),
        (midnight - timedelta(days=7), '1w'),
        (midnight - timedelta(days=14), '2w'),
        (midnight - timedelta(days=30), '1m'),
        (midnight - timedelta(days=91), '3m'),
        (midnight - timedelta(days=182), '6m'),
        (midnight - timedelta(days=365), '1y'),
        (midnight - timedelta(days=365 * 2), '2y'),
        (midnight - timedelta(days=365 * 5), '5y'),
    ]

    labels = [age[1] for age in ages]

    nrooms = sum([1 for series in house_data
                  if series[0] in ('temperatures',)])

    all_temps = [v for series in house_data
                 for t, v in series[3]
                 if series[0] in ('temperatures',)]
    min_temp = int(min(all_temps or [15]))
    max_temp = int(max(all_temps or [25]) + 1)

    min_p, max_p = 0, 100
    min_d, max_d = 0, len(labels) + 1

    props = {'whis': [1, 99],
             'sym': '|',
             'patch_artist': True,
             'medianprops': {'color': 'black',
                             'linewidth': 3},
             'flierprops': {'markeredgecolor': 'grey',
                            'markerfacecolor': 'grey',
                            'alpha': 0.5,
                            'markersize': 2}}

    fig = plt.figure(figsize=(width/dpi, height/dpi), dpi=dpi)

    gs = gridspec.GridSpec(2, nrooms, height_ratios=[1, 1])

    temp_col = 0
    perc_col = 0

    for series in house_data:
        if series[0] in ('temperatures',):
            ax_temp = plt.subplot(gs[0, temp_col], label='temp')
            ax_temp.set_ylim(bottom=min_temp, top=max_temp)
            ax_temp.set_title(title if temp_col == 0 else series.label)
            ax_temp.tick_params(left=temp_col == 0, labelleft=temp_col == 0,
                                bottom=False, labelbottom=False)

            temp_col += 1

            # temperature gradients

            z = list(range(min_temp, max_temp))
            normal = plt.Normalize(10, 40)  # mid point at 25°C
            cmap = plt.cm.coolwarm(normal(z))

            for t in z:
                ax_temp.fill([min_d, min_d, max_d, max_d],
                             [t, t+1, t+1, t],
                             facecolor=cmap[t - min_temp],
                             alpha=0.25)

            max_t = 0
            min_t = midnight
            temps = []

            for age in ages:
                max_t = min_t
                min_t = age[0]

                temps.append([v for t, v in series[3]
                              if min_t <= t < max_t])

            ax_temp.boxplot(list(reversed(temps)),
                            **props)

        if series[0] in ('humidity',):
            ax_perc = plt.subplot(gs[1, perc_col], label='perc')
            ax_perc.set_ylim(bottom=min_p, top=max_p)
            ax_perc.tick_params(left=perc_col == 0, labelleft=perc_col == 0)

            perc_col += 1

            # humidity gradients

            step = 10
            z = list(reversed(range(min_p, max_p+1, step)))
            normal = plt.Normalize(0, 100)  # reversed, mid point at 50%
            cmap = plt.cm.gist_earth(normal(z))

            for p in z:
                ax_perc.fill([min_d, min_d, max_d, max_d],
                             [p, p+step, p+step, p],
                             facecolor=cmap[(p - min_p) // step],
                             #edgecolor=BG_COLOR,
                             alpha=0.25)

            max_t = 0
            min_t = midnight
            perc = []

            for age in ages:
                max_t = min_t
                min_t = age[0]

                perc.append([v for t, v in series[3]
                             if min_t <= t < max_t])

            ax_perc.boxplot(list(reversed(perc)),
                            labels=list(reversed(labels)),
                            **props)

    fig.tight_layout()

    return fig
