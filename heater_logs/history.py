from datetime import datetime, timezone


def import_messages(log, legacy=False):
    last_ts = None
    last_topic = None
    last_message = None

    if legacy:
        ts_format = '[%Y-%m-%d %H:%M:%S]'
    else:
        ts_format = '[%Y-%m-%dT%H:%M:%S]'

    for line in log:
        # print(line.strip())

        ts1 = line[:21]
        topic, *message = line[22:].split(' ')
        ts = datetime.strptime(ts1, ts_format) \
                     .replace(tzinfo=timezone.utc) \
                     .astimezone() \
                     .replace(tzinfo=None)
        # print(ts1, ts)

        # special case to handle closing } in json message
        if topic.strip() in ['}', ']'] and not message:
            message = [topic]
            topic = ''

        if not topic and last_ts == ts:
            last_message.extend(message)
            continue

        if last_ts and last_topic and last_message:
            yield last_ts, last_topic, ' '.join(last_message)

        last_ts = ts
        last_topic = topic
        last_message = list(message)

    if last_ts and last_topic and last_message:
        yield last_ts, last_topic, ' '.join(last_message)
