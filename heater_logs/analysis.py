from scipy import stats


def median(nums):
    n = len(nums)

    if n == 0:
        return None

    if n == 1:
        return nums[0]

    if n % 2 == 1:
        return sorted(nums)[(n - 1) // 2]

    else:
        t = sorted(nums)
        return (t[n // 2] + t[n // 2 - 1]) / 2


def extrapolate(data):
    if not data:
        return

    yield (0, interpolate_y(data[0][0], data[0][1],
                            data[1][0], data[1][1], 0))

    yield from data

    yield (24 * 60 * 60,
           interpolate_y(data[-2][0], data[-2][1],
                         data[-1][0], data[-1][1], 24 * 60 * 60))


def interpolate_x(x0, y0, x1, y1, y2):
    return x0 + (y2 - y0) * (x1 - x0) / (y1 - y0)


def interpolate_y(x0, y0, x1, y1, x2):
    return y0 + (x2 - x0) * (y1 - y0) / (x1 - x0)


def exponential_average(data, window):
    alpha = 2 / (window + 1)
    avg = None

    for t, v in data:
        if avg is None:
            avg = v
        else:
            avg = (1 - alpha) * avg + alpha * v

        yield t, avg


def moving_sum(data, window):
    vals = []

    for t, v in data:
        vals = vals[-window:] + [v]

        yield t, sum(vals)


def area_under(data, bounces=3, cutoff=0):
    t0 = None
    area = 0
    bounce = bounces

    for t, v in data:
        if v > 0:
            if t0 is None:
                t0 = t
            area += v
            bounce = bounces

        else:
            bounce -= 1

            if t0 is not None and bounce <= 0:
                if area > cutoff:
                    yield t0, area
                t0 = None
                area = 0

    if t0 is not None:
        if area > cutoff:
            yield t0, area


def best_fit(T, V):
    slope, intercept, r_value, p_value, std_err \
        = stats.linregress([t.timestamp() for t in T], V)

    def v(t):
        return t.timestamp() * slope + intercept

    return v(T[0]), v(T[-1])


def find_cuts(data):
    avg = list(exponential_average(data, 60))

    error = [(t1, (v1 - v2)) for (t1, v1), (t2, v2) in zip(data, avg)]

    error_avg = list(moving_sum(error, 10))

    # TODO: use k-means or something to split noise from rain/watering
    # events
    error_area = list(area_under(error_avg, cutoff=200))

    return [data[0][0]] + [t for t, v in error_area] + [data[-1][0]]
